\documentclass[10pt,dvipsnames,aspectratio=169]{beamer}

\usetheme[progressbar=foot]{metropolis}
\usepackage{appendixnumberbeamer}

\usepackage{commons}
\include{macros}

\usepackage{tikz}

\usetikzlibrary{shapes,arrows,calc,patterns,positioning}
\usetikzlibrary{decorations.pathmorphing}

\title{Hardness and Independence of Polynomials}      
%\subtitle{}

\author{\textbf{Prerona Chatterjee}}
\date{November 26, 2021}
\institute{Tata Institute of Fundamental Research, Mumbai}
% \titlegraphic{\hfill\includegraphics[height=1.5cm]{logo.pdf}}

\begin{document}

\tikzset{%
	block/.style    = {draw, thick, rectangle, minimum height = 3em,
		minimum width = 2em},
	sum/.style      = {draw, circle, node distance = 2cm}, % Adder
	input/.style    = {coordinate}, % Input
	output/.style   = {coordinate} % Output
}
\tikzset{set/.style={draw,circle,inner sep=0pt,align=center}}
\newcommand{\suma}{\Large$+$}
\newcommand{\proda}{\Large$\times$}

\maketitle

\begin{frame}{Introduction}
    \begin{center}
        \textcolor{BrickRed}{Polynomials are important!}    
    \end{center}\pause
    
    \vspace{.5em}
    \begin{columns}
        \begin{column}{.4\textwidth}
            \uncover<2->{\begin{center}
                \textbf{Algebraic Circuit Complexity}
            \end{center}}
        \end{column}
        \begin{column}{.6\textwidth}
            \uncover<4->{\begin{center}
                \textbf{Identity Testing and Algebraic Independence}
            \end{center}}
        \end{column}
    \end{columns}

    \vspace{.5em}
    \begin{columns}
        \begin{column}{.4\textwidth}
            \uncover<3->{\begin{center}
                How \textcolor{OliveGreen}{efficiently} can a given computational model \textcolor{MidnightBlue}{compute} the given polynomial?
            \end{center}}
        \end{column}
        \begin{column}{.6\textwidth}
            \uncover<5->{\begin{center}
                Given a set of polynomials, how \textcolor{OliveGreen}{efficiently} can one check whether they are \textcolor{MidnightBlue}{algebraically independent}?\pause \pause \pause \pause
 
                \vspace{.5em}
                Given an algebraic circuit as input, how \textcolor{OliveGreen}{efficiently} can one check if it computes the \textcolor{MidnightBlue}{identically zero polynommial}?
            \end{center}}
        \end{column}
    \end{columns}
    \vspace{-1.5em}
\end{frame}

\section*{Part 1: Lower Bounds in Algebraic Circuit Complexity}

\begin{frame}{Algebraic Models of Computation}
	\begin{columns}
		\begin{column}{.4\textwidth}
		\begin{center}
			\begin{tikzpicture}[thick, node distance=2cm]
				\draw
				node [name=dummy] {}
				node [sum, right of=dummy] (top) {\suma}
				node [sum] at ($(top)+(-1,-1.5)$) (mult1) {\proda}
				node [sum] at ($(top)+(1,-1.5)$) (mult2) {\proda}
				node [sum] at ($(top)+(-1.5,-3)$) (sum1) {\suma}
				node [sum] at ($(top)+(0,-3)$) (sum2) {\suma}
				node [sum] at ($(top)+(1.5,-3)$) (sum3) {\suma}
				node [sum] at ($(top)+(-1.5,-4.5)$) (var1) {$x_1$}
				node [sum] at ($(top)+(0,-4.5)$) (var2) {$x_2$}
				node [sum] at ($(top)+(1.5,-4.5)$) (var3) {$x_3$};
				
				\draw[->](top) -- ($(top)+(0cm,1cm)$);
				\draw[->](mult1) -- node[left] {$\alpha_1$} (top);
				\draw[->](mult2) -- node[right] {$\alpha_2$} (top);
				\draw[->](sum1) -- (mult1);
				\draw[->](sum1) -- (mult2);
				\draw[->](sum2) -- (mult2);
				\draw[->](sum3) -- (mult1);
				\draw[->](var1) -- (sum1);
				\draw[->](var1) -- (sum2);
				\draw[->](var2) -- (sum1);
				\draw[->](var2) -- (sum3);
				\draw[->](var3) -- (sum2);  
				\draw[->](var3) -- (sum3); 
				
				\node at (2.5,.8) {$\ckt$};
			\end{tikzpicture}
		\end{center}
		\end{column}\pause
		\begin{column}{.6\textwidth}
		\begin{center}
			\begin{tikzpicture}[thick, node distance=1cm]
				\draw
				node [name=dummy] {}
				node [sum, right of=dummy] (top) {\suma}
				node [sum] at ($(top)+(-1,-1.5)$) (mult1) {\proda}
				node [sum] at ($(top)+(1,-1.5)$) (mult2) {\proda}
				node [sum] at ($(top)+(-2.25,-3)$) (sum1) {\suma}
				node [sum] at ($(top)+(-.75,-3)$) (sum2) {\suma}
				node [sum] at ($(top)+(0.75,-3)$) (sum3) {\suma}
				node [sum] at ($(top)+(2.25,-3)$) (sum4) {\suma};
				\node (var1) at ($(top)+(-2.75,-4.5)$) {$x_1$};
				\node (var2) at ($(top)+(-2,-4.5)$) {$x_2$};
				\node (var3) at ($(top)+(-1.25,-4.5)$) {$x_2$};
				\node (var4) at ($(top)+(-.5,-4.5)$) {$x_3$};
				\node (var5) at ($(top)+(0.5,-4.5)$) {$x_1$};
				\node (var6) at ($(top)+(1.25,-4.5)$) {$x_2$};
				\node (var7) at ($(top)+(2,-4.5)$) {$x_1$};
				\node (var8) at ($(top)+(2.75,-4.5)$) {$x_3$};
				
				\draw[->](top) -- ($(top)+(0cm,1cm)$);
				\draw[->](mult1) -- node[left] {$\alpha_1$} (top);
				\draw[->](mult2) -- node[right] {$\alpha_2$} (top);
				\draw[->](sum1) -- (mult1);
				\draw[->](sum2) -- (mult1);
				\draw[->](sum3) -- (mult2);
				\draw[->](sum4) -- (mult2);
				\draw[->](var1) -- (sum1);
				\draw[->](var2) -- (sum1);
				\draw[->](var3) -- (sum2);
				\draw[->](var4) -- (sum2);
				\draw[->](var5) -- (sum3);
				\draw[->](var6) -- (sum3);
				\draw[->](var7) -- (sum4);
				\draw[->](var8) -- (sum4);   
				
				\node at (1.5,.8) {$\form$};
			\end{tikzpicture}
		\end{center}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Algebraic Branching Programs}
	\begin{center}
	\begin{tikzpicture}[thick, node distance=2cm]
		\hspace{-.5em}
		\node[circle, draw=black] (start) {$s$};
		\node[circle, draw=black] (l11) at ($(start)+(2.6,1.5)$) {};
		\node[circle, draw=black] (l12) at ($(start)+(2.6,-.5)$) {};
		\node[circle, draw=black] (l21) at ($(start)+(5.2,0)$) {};
		\node[circle, draw=black] (l31) at ($(start)+(7.8,1.5)$) {};
		\node[circle, draw=black] (l32) at ($(start)+(7.8,-.5)$) {};
		\node[circle, draw=black] (l41) at ($(start)+(10.4,1.5)$) {};
		\node[circle, draw=black] (l42) at ($(start)+(10.4,-.5)$) {};
		\node[circle, draw=black] (end) at ($(start)+(13,1)$) {t};
		
		\draw[->](start) -- (l11);
		\only<2-4>{\draw[->](start) -- node[left] {\small{$(2x+3)$}} (l11)};
		\only<3>{\textcolor{red}{\draw[->](start) -- node[left] {\small{\textcolor{black}{$(2x+3)$}}} (l11);}}
		\draw[->](start) -- (l12);
		\draw[->](l11) -- (l21);
		\only<2-4>{\draw[->](l11) -- node[left] {\small{$(x+3y)$}} (l21)};
		\only<3>{\textcolor{red}{\draw[->](l11) -- node[left] {\small{\textcolor{black}{$(x+3y)$}}} (l21);}}
		\draw[->](l12) -- (l21);
		\draw[->](l21) -- (l31);
		\only<2-4>{\draw[->](l21) -- node[left] {\small{$(y+5)$}} (l31)};
		\only<3>{\textcolor{red}{\draw[->](l21) -- node[left] {\small{\textcolor{black}{$(y+5)$}}}(l31);}}
		\draw[->](l21) -- (l32);
		\draw[->](l31) -- (l42);
		\only<2,4>{\draw[->](l31) -- node[left] {\small{$10$}} (l42)};
		\only<3>{\textcolor{red}{\draw[->](l31) -- node[left] {\small{\textcolor{black}{$10$}}}(l42);}}
		\draw[->](l32) -- (l41);
		\draw[->](l32) -- (l42);
		\draw[->](l41) -- (end);
		\draw[->](l42) -- (end);
		\only<2-4>{\draw[->](l42) -- node[left] {\small{$(x+y+7)$}} (end)};
		\only<3>{\textcolor{red}{\draw[->](l42) -- node[left] {\small{\textcolor{black}{$(x+y+7)$}}} (end);}}

		\node at ($(start)+(13,-.25)$) {$\abp$};
	\end{tikzpicture}
	\end{center}\pause

	\vspace{1.5em}
	\begin{itemize}
		\item Label on each edge: \hspace{1em} An affine linear form in $\set{x_1, x_2, \ldots , x_n}$ \pause
		\item Polynomial computed by the path $p$ = $\mathsf{wt}(p)$: \hspace{1em} Product of the edge labels on $p$ \pause
		\item Polynomial computed by the ABP: \hspace{1em} $f_{\abp}(\vecx) = \sum_p \mathsf{wt}(p)$ 
	\end{itemize}
\end{frame}

\begin{frame}{Lower Bounds in Algebraic Circuit Complexity}

	\begin{center}
		\uncover<1->{\textbf{Objects of Study}: Polynomials over $n$ variables of degree $d$.}
	\end{center}

	\begin{columns}
		\begin{column}{.65\textwidth}
			
			\vspace{.75em}
			\uncover<3->{$\VF$: Polynomials computable by formulas of size $\poly(n,d)$.}

			\vspace{.75em}
			\uncover<4->{$\VBP$: Polynomials computable by ABPs of size $\poly(n,d)$.}

			\vspace{.75em}
			\uncover<2->{$\VP$: Polynomials computable by circuits of size $\poly(n,d)$.}

			\vspace{.75em}
			\uncover<5->{$\VNP$: Explicit Polynomials}

			\vspace{1em}
			\begin{center}
				\uncover<6->{Are the inclusions tight?}
			\end{center}
		\end{column}
		\begin{column}{.35\textwidth}

			\vspace{1em}
			\begin{tikzpicture}[scale=1.1,transform shape]
				\uncover<5->{\node at (1.3,-2.1) {$\VNP$};
				\node[rectangle,text width=3.5cm,text height=3.5cm,draw] at (0,-0.6) (vnp) {};}
				\uncover<2->{\node[set,text width=3cm,label={[below=70pt of vp]$\VP$}] (vf) at (0,-0.4)  (vp) {};}
				\uncover<4->{\node[set,text width=2cm,label={[below=40pt of vbp]$\VBP$}] (vbp) at (0,-0.2)  {};}
				\uncover<3->{\node[set,text width=1cm] (vf) at (0,0) {$\VF$};}
			\end{tikzpicture}
		\end{column}
	\end{columns}

	\vspace{1.2em}
	\uncover<7->{\textbf{Central Question}: Find \textcolor{BrickRed}{explicit} polynomials that cannot be computed by \textcolor{MidnightBlue}{efficient} circuits.}
	\vspace{-2em}
\end{frame}

\begin{frame}{Lower Bounds for General Models}
	\begin{center}
		\textbf{General Circuits}

		\textbf{[Baur-Strassen]}: Any algebraic circuit computing $\sum_{i=1}^{n} x_i^d$ requires $\Omega(n \log d)$ wires. \pause
	\end{center}
	
	\vspace{.75em}
	\begin{center}
		\textbf{Homogeneous ABPs} 
		
		\textbf{[Kumar]}: Any ABP with $(d+1)$ layers computing $\sum_{i=1}^{n} x_i^d$ requires $\Omega(nd)$ vertices.\pause
	\end{center}
		
	\vspace{.75em}	
	\begin{center}
		\textbf{General ABPs} 
		
		\textbf{[\textcolor{BrickRed}{C}-Kumar-She-Volk]}: Any ABP computing $\sum_{i=1}^{n} x_i^d$ requires $\Omega(nd)$ vertices.
	\end{center}	
	\vspace{-1em}	
\end{frame}

\begin{frame}{Lower Bounds for General Models (contd.)}
	\begin{center}
		\textbf{General Formulas}

		\vspace{.5em}
		\textbf{[Kalorkoti]}: Any formula computing the $n^2$-variate $\Det{n}(\vecx)$ requires $\Omega(n^3)$ wires.\pause

		\vspace{1em}
		\textbf{[Shpilka-Yehudayoff]} (using Kalorkoti's method): There is an $n$-variate multilinear polynomial such that any formula computing it requires $\Omega(n^2/\log n)$ wires.\pause

		\vspace{1.5em}
		\textbf{[\textcolor{BrickRed}{C}-Kumar-She-Volk]}: Any formula computing $\mathsf{ESym}_{n,0.1n}(\vecx)$ requires $\Omega(n^2)$ vertices, where
		\[
			\esym{n}{d}(\vecx) = \sum_{i_1 < \cdots < i_d \in [n]} x_{i_1} \cdots x_{i_d}.
		\]
	\end{center}
	\vspace{-2em}
\end{frame}

\begin{frame}{Some Remarks}
    \textbf{The ABP Lower Bound}
    \begin{enumerate}
		\item If the edges are allowed to be polynomials of degree $\leq \Delta$, we get an $\Omega(n^2/\Delta)$ lower bound.\pause
		\item In the unlayered case, if the edges are labelled by polynomials of degree at most $\Delta$, the lower bound we get is $\Omega(n \log n/ \Delta \log \log n)$. \pause
	\end{enumerate}

    \vspace{.5em}
    \textbf{The Formula Lower Bound}
    \begin{enumerate}
		\item SY had shown that any formula computing $\sum_{i=1}^{n} \sum_{j=1}^{n} x_i^j y_j$ requires $\Omega(n^2)$ wires. \pause
		\item It is trivial to show an $nd$ lower bound for polynomials over $n$ variables that have \emph{individual degree} at least $d$. \pause
		\item Multilinearisation of the SY polynomial gives an $\Omega(n^2/\log n)$ lower bound.\pause
		\item Kalorkoti's method can not give a better bound against multilinear polynomials [Jukna].
	\end{enumerate}
    \vspace{-2em}
\end{frame}

\begin{frame}{Proof Overview: The ABP Lower Bound}
	\textbf{Step 0} ([Kumar]): \underline{Look at the homogeneous case}
	\begin{center}
		Any ABP with $(d+1)$ layers computing $\sum_{i=1}^{n} x_i^d$ has $\Omega(nd)$ vertices.\pause
	\end{center}

	\vspace{1em}
	\textbf{Step 1}: \underline{Generalise above statement to get the base case}\\
	\begin{center}
		Any ABP with $(d+1)$ layers \pause computing a polynomial of the form 
	\[
		f = \sum_{i=1}^n x_i^d + \sum_{i=1}^r A_i(\vecx) \cdot B_i(\vecx) + \delta(\vecx)
	\] \pause
	where \hspace{1.5em} $A_i(\zero) = \zero = B_i(\zero)$ \hspace{1.5em} and \hspace{1.5em} $\deg(\delta(\vecx)) < d$, \hspace{1.5em} \pause has at least 
	\[
		\text{\textcolor{BrickRed}{$((n/2) - r) \cdot (d-1)$} \hspace{1em} vertices}.
	\]
	\end{center}
	\vspace{-3em}
\end{frame}

\begin{frame}{Proof Overview: The ABP Lower Bound}
	\textbf{Step 2}: \underline{Iteratively reduce to Base Case}\\
	\vspace{.5em}
	In each iteration, reduce the number of layers till it becomes $(d+1)$ such that\pause
	\vspace{.5em}
	\begin{itemize}
		\item the number of layers is reduced by a constant fraction,\pause
		\item the size does not increase, \pause
		\item the polynomial being computed continues to look like
		\[
			f_{\ell +1} = \sum_{i=1}^{n} x_i^d + \sum_{i=1}^{r_{\ell+1}} A_i(\vecx) \cdot B_i(\vecx) + \delta_{\ell +1}(\vecx)
		\]
		where \hspace{1em} $A_i(\zero) = \zero = B_i(\zero)$ \hspace{1em} and \hspace{1em} $\deg(\delta_{\ell + 1}(\vecx)) < d$, \pause
		\item number of error terms collected is small. 
	\end{itemize}
\end{frame}

\begin{frame}
	\begin{center}
		\large{\textbf{Questions?}}
	\end{center}
\end{frame}

\begin{frame}{The Non-Commutative Setting}
	\[
		f(x,y) = (x+y)^2 = (x+y) \times (x+y) = x^2 + xy + yx + y^2 \neq x^2 + 2 xy + y^2 \text{\pause}
	\]
	
	\begin{columns}
		\begin{column}{.45\textwidth}
		\begin{center}
			\begin{tikzpicture}[thick, node distance=2cm]
				\draw
				node [name=dummy] {}
				node [sum, right of=dummy] (top) {\suma}
				node [sum] at ($(top)+(-1,-1.25)$) (mult1) {\proda}
				node [sum] at ($(top)+(1,-1.25)$) (mult2) {\proda}
				node [sum] at ($(top)+(-1.5,-2.5)$) (sum1) {\suma}
				node [sum] at ($(top)+(0,-2.5)$) (sum2) {\suma}
				node [sum] at ($(top)+(1.5,-2.5)$) (sum3) {\suma}
				node [sum] at ($(top)+(-1.5,-3.75)$) (var1) {$x_1$}
				node [sum] at ($(top)+(0,-3.75)$) (var2) {$x_2$}
				node [sum] at ($(top)+(1.5,-3.75)$) (var3) {$x_3$};
				
				\draw[->](top) -- ($(top)+(0cm,1cm)$);
				\draw[->](mult1) -- node[left] {$\alpha_1$} (top);
				\draw[->](mult2) -- node[right] {$\alpha_2$} (top);
				\draw[->](sum1) -- (mult1);
				\draw[->](sum1) -- (mult2);
				\draw[->](sum2) -- (mult2);
				\draw[->](sum3) -- (mult1);
				\draw[->](var1) -- (sum1);
				\draw[->](var1) -- (sum2);
				\draw[->](var2) -- (sum1);
				\draw[->](var2) -- (sum3);
				\draw[->](var3) -- (sum2);  
				\draw[->](var3) -- (sum3); 
				
				\node at (2.5,.8) {$\ckt$};
				\node at (.5,.4) {\Large{\textcolor{MidnightBlue}{$\VP_{\text{nc}}$}}};
				\uncover<3->{\node at (6.5,0) {\Large{\textcolor{OliveGreen}{$\boxed{\VP_{\text{nc}} \supseteq \VF_{\text{nc}}}$}}};}
			\end{tikzpicture}
		\end{center}
		\end{column}
		\begin{column}{.55\textwidth}
		\begin{center}
			\begin{tikzpicture}[thick, node distance=1cm]
				\uncover<2->{\draw
				node [name=dummy] {}
				node [sum, right of=dummy] (top) {\suma}
				node [sum] at ($(top)+(-1,-1.25)$) (mult1) {\proda}
				node [sum] at ($(top)+(1,-1.25)$) (mult2) {\proda}
				node [sum] at ($(top)+(-2.25,-2.5)$) (sum1) {\suma}
				node [sum] at ($(top)+(-.75,-2.5)$) (sum2) {\suma}
				node [sum] at ($(top)+(0.75,-2.5)$) (sum3) {\suma}
				node [sum] at ($(top)+(2.25,-2.5)$) (sum4) {\suma};
				\node (var1) at ($(top)+(-2.75,-3.75)$) {$x_1$};
				\node (var2) at ($(top)+(-2,-3.75)$) {$x_2$};
				\node (var3) at ($(top)+(-1.25,-3.75)$) {$x_2$};
				\node (var4) at ($(top)+(-.5,-3.75)$) {$x_3$};
				\node (var5) at ($(top)+(0.5,-3.75)$) {$x_1$};
				\node (var6) at ($(top)+(1.25,-3.75)$) {$x_2$};
				\node (var7) at ($(top)+(2,-3.75)$) {$x_1$};
				\node (var8) at ($(top)+(2.75,-3.75)$) {$x_3$};
				
				\draw[->](top) -- ($(top)+(0cm,1cm)$);
				\draw[->](mult1) -- node[left] {$\alpha_1$} (top);
				\draw[->](mult2) -- node[right] {$\alpha_2$} (top);
				\draw[->](sum1) -- (mult1);
				\draw[->](sum2) -- (mult1);
				\draw[->](sum3) -- (mult2);
				\draw[->](sum4) -- (mult2);
				\draw[->](var1) -- (sum1);
				\draw[->](var2) -- (sum1);
				\draw[->](var3) -- (sum2);
				\draw[->](var4) -- (sum2);
				\draw[->](var5) -- (sum3);
				\draw[->](var6) -- (sum3);
				\draw[->](var7) -- (sum4);
				\draw[->](var8) -- (sum4);   
				
				\node at (1.5,.8) {$\form$};}
				\node at (4,.3) {\Large{\textcolor{MidnightBlue}{$\VF_{\text{nc}}$}}};
			\end{tikzpicture}
		\end{center}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Lower Bounds for General Non-Commutative Models}
	\begin{center}
		Is there an explicit polynomial that is outside $\VP_{\text{nc}}$? \pause
		\uncover<3->{What about $\VF_{\text{nc}}$?}
	\end{center}

	\vspace{.75em}
	\textbf{Circuits} [Baur - Strassen]: $\Omega(n \log d)$ for an $n$-variate, degree $d$ polynomial. \pause \pause

	\vspace{.5em}
	\textbf{Formulas} [Nisan]: $2^{\Omega(n)}$ for a $2$-variate, degree $n$ polynomial in $\VP_{\text{nc}}$. \pause So, \textcolor{OliveGreen}{$\VF_{\text{nc}} \neq \VP_{\text{nc}}$}. \pause

	\vspace{.75em}
	\begin{center}
		But the proof is via a lower bound against non-commutative \textcolor{MidnightBlue}{Algebraic Branching Programs}.
	\end{center} \pause

	\begin{center}
		\uncover<8->{\textcolor{OliveGreen}{$\VF_{\text{nc}}$} $\subseteq$} \textcolor{MidnightBlue}{$\VBP_{\text{nc}}$} \uncover<8->{$\subseteq$ \textcolor{OliveGreen}{$\VP_{\text{nc}}$}}
	\end{center} \pause \pause

	\begin{center}
		So Nisan actually showed that $\VBP_{\text{nc}} \neq \VP_{\text{nc}}$.
	\end{center} 

	\vspace{-.5em}
\end{frame}

\begin{frame}{The ABP vs Formula Question}
	\textbf{The Question} [Nisan]:

	\vspace{-2.75em}
	\begin{center}
		Is $\VF_{\text{nc}} = \VBP_{\text{nc}}$?
	\end{center}\pause

	\vspace{.25em}
	\textbf{Note}: Every \textcolor{MidnightBlue}{monomial} in a non-commutative polynomial $f(x_1, \ldots, x_n)$ can be thought of as a \textcolor{MidnightBlue}{word} over the underlying variables $\set{x_1, \ldots, x_n}$. \pause

	\vspace{.5em}
	\textbf{Definitions}\pause
	\hspace{2em} Let $\set{X_1, \ldots, X_m}$ be a partition of the variables into buckets.\pause

	\vspace{.25em}
	\textcolor{OliveGreen}{Abecedarian Polynomials}: Polynomials in which every monomial has the form $X_1^* X_2^* \cdots X_m^*$. \pause

	\uncover<6->{
	\only<-11>{\uncover<-10>{
	\vspace{-.25em}
	\begin{center}
		$X_1 \uncover<7->{= \{x_1\}}$  
		\hspace{2em} $\uncover<6->{X_2} \uncover<7->{= \{x_2\}}$ 
		\pause \pause
		\hspace{6em} \textcolor{OliveGreen}{$x_1x_1x_2$} \pause
		\hspace{2em} \textcolor{OliveGreen}{$x_2$} \pause
		\hspace{2em} \textcolor{BrickRed}{$x_1x_2x_1$}
	\end{center}}}

	\only<12->{\textcolor{OliveGreen}{Syntactically Abecedarian Formulas}: Non-commutative formulas with a syntactic restriction that makes them naturally compute abecedarian polynomials.}}
	

	\vspace{.25em}
	\uncover<13->{\begin{center}
		\textbf{Main Result}:\\
		\textcolor{BrickRed}{There is a tight superpolynomial separation between \emph{abecedarian} formulas and ABPs.}
	\end{center}}

	\vspace{-1.5em}
\end{frame}

\begin{frame}{Proof Idea}
	\textbf{The Statement}:
	There is an explicit $n^2$-variate, degree-$d$ abcd-polynomial $f_{n,d}(\textbf{x})$ such that \pause
	\begin{itemize}
		\item \textcolor{OliveGreen}{abcd-ABP Upper Bound}: the abcd-ABP complexity of $f_{n, d}(\textbf{x})$ is $\Theta(nd)$;\pause
		\item \textcolor{BrickRed}{abcd-Formula Lower Bound}: the abcd-formula complexity of $f_{n, \log n}(\textbf{x})$ is $n^{\Theta(\log \log n)}$.\pause
	\end{itemize}

	\vspace*{1.5em}
	\textbf{The Proof Idea}:
	\begin{enumerate}
		\uncover<6->{\item Use \textcolor{MidnightBlue}{low degree} to make the abcd-formula structured.}
		\uncover<7->{\item Use the structured formula to \textcolor{MidnightBlue}{amplify degree} while keeping the structure intact.}
		\uncover<8->{\item Convert the structured abcd-formula into a \textcolor{MidnightBlue}{homogeneous multilinear formula}.}
		\uncover<5->{\item Use \textcolor{MidnightBlue}{known lower bound} against homogeneous multilinear formulas [HY11].}
	\end{enumerate}
\end{frame}

\begin{frame}
	\begin{center}
		\large{\textbf{Questions?}}
	\end{center}
\end{frame}

\section*{Part 2: Identity Testing and Algebraic Indepndence}
\begin{frame}{Algebraic Independence}
	In the vector space $\R^3$ over $\R$,
	\[
		\uncover<3>{\text{\textcolor{BrickRed}{$1$}} \times}(1,0,1) \uncover<3>{+ \text{\textcolor{BrickRed}{$2$}} \times} (0,1,0) \uncover<3>{- \text{\textcolor{BrickRed}{$1$}} \times} (1,2,1) \uncover<3>{= \text{\textcolor{BrickRed}{$0$}}}
	\]
	\uncover<4->{are \textcolor{OliveGreen}{linearly dependent.}}\\
	
	\vskip 2em
		
	\uncover<5->{In the space of bi-variate polynomials over $\C$,
	\[
	x^2 \uncover<6>{\quad \times \quad} y^2 \uncover<6>{\quad - \quad(} xy \uncover<6>{)\text{\textcolor{BrickRed}{$^2$}}} \quad \uncover<6>{=} \quad \uncover<6>{\text{\textcolor{BrickRed}{$0$}}}
	\]}
	\uncover<7->{are \textcolor{OliveGreen}{algebraically dependent.}}
\end{frame}

\begin{frame}{Algebraic Independence}
	A set of polynomials $\set{f_1, \ldots, f_k} \subseteq \F[\x]$ are said to be \textcolor{OliveGreen}{algebraically dependent} if there exists $\text{\textcolor{BrickRed}{$A$}} \in \F[y_1, \ldots, y_k]$ \pause such that 
	\[
        \text{\textcolor{BrickRed}{$A$}}(y_1, \ldots, y_k) \neq \zero; \qquad \text{\textcolor{BrickRed}{$A$}}(f_1, \ldots, f_k) = \zero.
    \]\pause
	Otherwise, they are said to be \textcolor{MidnightBlue}{algebraically independent}.\pause
	
	\vspace{1.5em}
	\textbf{Note}: The underlying field is very important. \pause 
    For any prime $p$,	
	\[
        x^p + y^p \qquad x+y
    \]\pause

    \vspace{-1.5em}
	\begin{itemize}
		\item are \textcolor{MidnightBlue}{algebraically independent} over $\C$. \pause
		\item are \textcolor{OliveGreen}{algebraically dependent} over $\F_p$, \pause \qquad since \qquad $x^p + y^p = (x+y)^p$.
	\end{itemize}
    \vspace{-1em}
\end{frame}

\begin{frame}{Algebraic Rank}
	\begin{columns}
		\uncover<2->{\begin{column}{.2\textwidth}
			\begin{tikzpicture}[thick]
			\draw (0,0) ellipse (1 and 1.5);
			\node at (0,0) {$\R^3$};
			\end{tikzpicture}
		\end{column}}
		\begin{column}{.7\textwidth}
			\begin{itemize}
				\item \textcolor{BrickRed}{Linear rank} of $S = \set{v_1, \ldots, v_m} \subseteq \mathbb{V}$ is the size of the largest \textcolor{MidnightBlue}{linearly independent} subset of $S$. \pause
				\item \textcolor{OliveGreen}{Linear rank} of $\set{(1,0,1), (0,1,0), (1,2,1)}$ is $2$. \pause
			\end{itemize}
		\end{column}
	\end{columns}

    \vspace{.5em}
	\uncover<3->{\begin{columns}
		\begin{column}{.7\textwidth}
			\begin{itemize}
				\item \textcolor{BrickRed}{Algebraic rank} of $S = \set{f_1, \ldots, f_m} \subseteq \mathbb{\F[\vecx]}$ is the size of the largest \textcolor{MidnightBlue}{algebraically independent} subset of $S$.
				\only<3-4>{\uncover<4>{\item \textcolor{OliveGreen}{Algebraic rank} of $\set{x^p + y^p, x+y}$ is \textcolor{BrickRed}{$1$}}}
				\only<5>{\item \textcolor{OliveGreen}{Algebraic rank} of $\set{x^p + y^p, x+y}$ is \textcolor{BrickRed}{$2$}}
			\end{itemize}
		\end{column}
		\begin{column}{.2\textwidth}
			\begin{tikzpicture}[thick]
			\uncover<4-5>{\draw (0,0) ellipse (1 and 1.5);}
			\uncover<4>{\node at (0,0) {$\F_p[x,y]$};}
			\only<5>{\node at (0,0) {$\C[x,y]$};}
			\end{tikzpicture}
		\end{column}
	\end{columns}}
    \vspace{-.5em}
\end{frame}

\begin{frame}{Rank Preserving Maps}
	\begin{block}{Basis in Linear Algebra}
        \vspace{.3em}
        Given a set of vectors $\set{v_1, v_2, \ldots, v_m}$ with \textcolor{OliveGreen}{linear rank $k$}, there is a basis of \textcolor{MidnightBlue}{size $k$}. 
    \end{block}\pause
	
    \vspace{1em}
	\begin{block}{Faithful Maps}
        \vspace{.3em}
		Given a set of polynomials $\set{\fm}$ with algebraic rank $k$, a map 
		\[
            \phi: \set{\x} \to \F[\yk]
        \] 
        is said to be a \textcolor{BrickRed}{faithful} map if the \textcolor{OliveGreen}{algebraic rank} of $\set{f_1 \circ \phi, f_2 \circ \phi, \ldots, f_m \circ \phi}$ is also $k$.
	\end{block}\pause

	\vspace{.5em}
	\begin{center}
		\textbf{Question}: Can we construct faithful maps efficiently?\\\pause
		\vspace{.5em}
		\textbf{Bonus}: Helps in polynomial identity testing.
	\end{center}
    \vspace{-1em}
\end{frame}

\begin{frame}{Polynomial Identity Testing}
	\textbf{Given}: Circuit $\ckt$ that computes an $n$-variate, degree $d$ polynomial\\
	\textbf{Goal}: Check whether $\ckt \cong$ Zero Polynomial.
    
    \vspace{1.5em}
	\begin{columns}
		\begin{column}{.3\textwidth}
			\begin{tikzpicture}[thick]
			\uncover<1->{\draw (0,0) circle (.3);
				\draw[->] (0,.3) -- (0,1);
				\node at (.5,.8) {$\ckt$};}
			\uncover<1-3>{
				\draw (-.2,-.25) -- (-1.6,-3.2) -- (1.6,-3.2) -- (.2,-.25);
				\draw (-.3,-1.5) circle (.3);
				\node at (-.3,-1.5) {$\times$};
				\draw (.5,-2.2) circle (.3);
				\node at (.5,-2.2) {$+$};
				\node at (-1.3,-3.7) {$x_1$};
				\node at (-.8,-3.7) {$x_2$};
				\node at (-.1,-3.6) {$\cdots$};
				\node at (.5,-3.6) {$\cdots$};
				\node at (1.3,-3.7) {$x_n$};
				\draw (-1.3,-3.4) -- (-1.3,-3);
				\draw (-.8,-3.4) -- (-.8, -3);
				\draw (1.3,-3.4) -- (1.3,-3);}
			\uncover<4->{
				\node at (0,-1.2) {$\ckt'$};
				\draw (-.2,-.25) -- (-1.5,-2) -- (1.5,-2) -- (.2,-.25);
				\draw (-1.1,-1.9) -- (-1.1,-2.2) -- (-1.6,-2.8) -- (-.6,-2.8) -- (-1.1,-2.2);
				\node at (-1.1,-2.55) {\small{$f_1$}};
				\node at (-.2,-2.55) {$\cdots$};
				\node at (.2,-2.55) {$\cdots$};
				\node at (1.1,-2.55) {\small{$f_m$}};
				\draw (1.1,-1.9) -- (1.1,-2.2) -- (1.6,-2.8) -- (.6,-2.8) -- (1.1,-2.2);
				\draw (-1.4,-2.7) -- (-1.4,-3);
				\draw (-1.3,-2.7) -- (-1.3,-3);
				\node at (-1.05,-3) {$\cdots$};
				\draw (-.8,-2.7) -- (-.8,-3);
				\draw (1.4,-2.7) -- (1.4,-3);
				\draw (1.3,-2.7) -- (1.3,-3);
				\node at (1.05,-3) {$\cdots$};
				\draw (.8,-2.7) -- (.8,-3);
				\draw (-1.3,-3.6) circle (.2);
				\draw (-.5,-3.6) circle (.2);
				\node at (.1,-3.6) {$\cdots$};
				\node at (.6,-3.6) {$\cdots$};
				\draw (1.3,-3.6) circle (.2);
				\draw (-1.3,-3.4) -- (-1.3,-3.2);
				\draw (-.5,-3.4) -- (-.5, -3.2);
				\draw (1.3,-3.4) -- (1.3,-3.2);
				\node at (-1.3,-3.6) {\tiny{$x_1$}};
				\node at (-.5,-3.6) {\tiny{$x_2$}};
				\node at (1.3,-3.6) {\tiny{$x_n$}};}
			\end{tikzpicture}
		\end{column}
		\begin{column}{.7\textwidth}
			\uncover<2->{\textbf{Trivial Upperbound}: \textcolor{BrickRed}{$(d+1)^n$}
            \pause \pause
			\begin{tabbing}
				\textbf{Approach}: \= Reduce no. of variables\\
				\> Keep degree under control\\
				\> Preserve non-zeroness
			\end{tabbing}}
			\uncover<4->{\textbf{Special Case}: $\ckt = \ckt'(\fm)$ where \textcolor{OliveGreen}{algebraic rank} of $\set{f_1, \ldots, f_m} = k$, and 
				\[
                    k \ll n
                \]}
            
            \vspace{-1em}
			\uncover<5->{
				\begin{center}
					\textbf{Q}: Can the upperbound be made $\approx (d+1)^k$?
			\end{center}}
		\end{column}
	\end{columns}

    \vspace{-.5em}
\end{frame}

\begin{frame}{Faithful Maps \& PIT [Beecken-Mittman-Saxena, Agrawal-Saha-Saptharishi-S]}
	\begin{columns}
		\begin{column}{.3\textwidth}			
			\begin{tikzpicture}[thick]
			\draw (0,0) circle (.3);
			\draw[->] (0,.3) -- (0,1);
			\node at (.75,.8) {$\ckt \circ \phi$};
			\node at (0,-1.2) {$\ckt'$};
			\draw (1.3,-3.6) circle (.2);
			\draw (-.2,-.25) -- (-1.5,-2) -- (1.5,-2) -- (.2,-.25);
			\draw (-1.1,-1.9) -- (-1.1,-2.2) -- (-1.6,-2.8) -- (-.6,-2.8) -- (-1.1,-2.2);
			\node at (-1.1,-2.55) {\small{$f_1$}};
			\node at (-.2,-2.55) {$\cdots$};
			\node at (.2,-2.55) {$\cdots$};
			\node at (1.1,-2.55) {\small{$f_m$}};
			\draw (1.1,-1.9) -- (1.1,-2.2) -- (1.6,-2.8) -- (.6,-2.8) -- (1.1,-2.2);
			\draw (-1.4,-2.7) -- (-1.4,-3);
			\draw (-1.3,-2.7) -- (-1.3,-3);
			\node at (-1.05,-3) {$\cdots$};
			\draw (-.8,-2.7) -- (-.8,-3);
			\draw (1.4,-2.7) -- (1.4,-3);
			\draw (1.3,-2.7) -- (1.3,-3);
			\node at (1.05,-3) {$\cdots$};
			\draw (.8,-2.7) -- (.8,-3);
			\draw (-1.3,-3.6) circle (.2);
			\draw (-.5,-3.6) circle (.2);
			\node at (.1,-3.6) {$\cdots$};
			\node at (.6,-3.6) {$\cdots$};
			\draw (1.3,-3.6) circle (.2);
			\draw (-1.3,-3.4) -- (-1.3,-3.2);
			\draw (-.5,-3.4) -- (-.5, -3.2);
			\draw (1.3,-3.4) -- (1.3,-3.2);
			\node at (-1.3,-3.6) {\tiny{$\phi_1$}};
			\node at (-.5,-3.6) {\tiny{$\phi_2$}};
			\node at (1.3,-3.6) {\tiny{$\phi_n$}};
			\draw (-1.4,-3.75) -- (-1.6,-4.1) -- (-1,-4.1) -- (-1.2,-3.75);
			\draw (-.6,-3.75) -- (-.8,-4.1) -- (-.2,-4.1) -- (-.4,-3.75);
			\draw (1.2,-3.75) -- (1,-4.1) -- (1.6,-4.1) -- (1.4,-3.75);
			\draw (-1.5,-4.05) -- (-1.5,-4.2);
			\draw (-1.4,-4.05) -- (-1.4,-4.2);
			\draw (-1.1,-4.05) -- (-1.1,-4.2);
			\draw (-.7,-4.05) -- (-.7,-4.2);
			\draw (-.6,-4.05) -- (-.6,-4.2);
			\draw (-.3,-4.05) -- (-.3,-4.2);
			\draw (1.1,-4.05) -- (1.1,-4.2);
			\draw (1.2,-4.05) -- (1.2,-4.2);
			\draw (1.5,-4.05) -- (1.5,-4.2);
			\node at (-1.3,-5) {$y_1$};
			\node at (-.7,-5) {$y_2$};
			\node at (-.05,-5) {$\cdots$};
			\node at (.5,-5) {$\cdots$};
			\node at (1.3,-5) {$y_k$};
			\draw (-1.3,-4.4) -- (-1.3,-4.7);
			\draw (-.7,-4.4) -- (-.7, -4.7);
			\draw (1.3,-4.4) -- (1.3,-4.7);
			\end{tikzpicture}
		\end{column}
		\begin{column}{.7\textwidth}
			\textbf{Property required}: $\ckt \neq \zero \implies \ckt \circ \phi \neq \zero$ \pause
			
			\vspace{1em}
			If $k = m$ and $\ckt' \neq \zero$,
			
			\vspace{-.5em}
			\[
			\ckt'(f_1, \ldots, f_k) \neq \zero \pause
			\]
			
			\vspace{-1em}
			Since $\phi$ is faithful, 
			
			\vspace{-.5em}
			\[
				\ckt \circ \phi = \ckt' (f_1 \circ \phi, \ldots, f_k \circ \phi) \neq \zero \pause
			\]
			
			\vspace{-1em}
			Thus,
			\[\ckt \neq \zero \implies \ckt \circ \phi \neq \zero\]\pause
			
			\textbf{Fact}: Even when $k < m$, if $\phi$ is faithful, $\ckt \neq \zero \implies \ckt \circ \phi \neq \zero$.
		\end{column}
	\end{columns}
    \vspace{-2em}
\end{frame}

\begin{frame}{The Question}
	Given a set of polynomials $\set{\fm} \subseteq \F[x_1, \ldots, x_n]$, we want to construct a map
	\[
        \phi: \set{\x} \to \F[\yk]
    \] 
	such that 
	\[
		\text{\textcolor{BrickRed}{$\algrank$}}(f_1(\phi), f_2(\phi), \ldots, f_m(\phi)) = \text{\textcolor{BrickRed}{$\algrank$}}(f_1, f_2, \ldots, f_m)
	\]\pause
	
	\textbf{Fact}: A random affine transformation is a faithful map
	\[
        \phi : x_i = \sum_{j=1}^{k}s_{ij}y_j + a_i
    \] \pause
	
	\textbf{Question}: Can we construct faithful maps \textcolor{OliveGreen}{deterministically}?
    \vspace{-1.5em}
\end{frame}

\begin{frame}{Characteristic Zero Fields [B-M-S, A-S-S-S]}
	\textbf{Step 1}: Capture algebraic rank via linear rank \uncover<4->{of the \textcolor{OliveGreen}{Jacobian}}\pause

	For $\set{\fm} \subseteq \F[\x]$ and $\vecf = (\fm)$, 
	\[\J_\vecx(\vecf) = 
	    \begin{bmatrix}
            \partial_{x_1}(f_1) & \partial_{x_1}(f_2) & \ldots & \partial_{x_1}(f_n)\\
            \partial_{x_2}(f_1) & \partial_{x_2}(f_2) & \ldots & \partial_{x_2}(f_n)\\
            \vdots & \vdots & \ddots & \vdots\\
            \partial_{x_n}(f_1) & \partial_{x_n}(f_2) & \ldots & \partial_{x_n}(f_m)\\
		\end{bmatrix}
    \]\pause
	\begin{block}{The Jacobian Criterion [Jacobi]}
        \vspace{.5em}
		If $\F$ has characteristic zero, the algebraic rank of $\set{\fm}$ is equal to the linear rank of its Jacobian matrix.
	\end{block}
\end{frame}

\begin{frame}{Characteristic Zero Fields [B-M-S, A-S-S-S]}
	\textbf{Step 2}: Start with a generic linear transformation.\qquad 
	\only<1-7>{$\phi : x_i = \sum_{j=1}^{k}s_{ij}y_j + a_i$}
	\only<8->{\textcolor{BrickRed}{$\phi : x_i = \sum_{j=1}^{k}s^{ij}y_j + a_i$}}\pause
    
    \[
        \begin{bmatrix}
		\text{ } & \text{ } & \text{ }\\
		\text{ } & \text{ } & \text{ }\\
		\text{ } & \J_\vecy(\vecf(\phi)) & \text{ }\\ 
		\text{ } & \text{ } & \text{ }\\
		\text{ } & \text{ } & \text{ }\\
		\end{bmatrix} \pause
		=
		\begin{bmatrix}
		& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &\\ 
		& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &\\
		& \text{ } & \text{ } & \phi(\J_\vecx(\vecf)) & \text{ } & \text{ } &\\
		& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &\\
		& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &
		\end{bmatrix}
		\times 
		\begin{bmatrix}
		& \text{ } & \text{ } & \text{ } &\\
		& \text{ } & \text{ } & \text{ } &\\
		& \text{ } & \text{ } & \text{ } &\\
		& \text{ } & M_\phi & \text{ } &\\ 
		& \text{ } & \text{ } & \text{ } &\\
		& \text{ } & \text{ } & \text{ } &\\
		& \text{ } & \text{ } & \text{ } &\\
		\end{bmatrix}
    \]\pause

    \vspace{-.5em}
	\textbf{What we need:} $\phi$ such that
	\begin{itemize}
		\item $\rank(\J_\vecx(\vecf)) = \rank(\phi(\J_\vecx(\vecf)))$ \pause : Can be done if $f_i$s are structured \pause
		\item $M_\phi$ preserves rank \pause : True if $\set{M_\phi[i,j] = s^{ij}}$ $\ldots \ldots \ldots$ [GR05]
	\end{itemize}
    \vspace{-1em}
\end{frame}

\begin{frame}{What happens over Finite Characteristic Fields?}
	The Jacobian Criterion is \textcolor{BrickRed}{\textbf{false}} over finite characteristic fields.\pause
	
	\vspace{1em}
	\textbf{Taylor Expansion}: For any $f \in \F[\x]$ and $\vecz \in \F^n$,
	\[
        f(\vecx + \vecz) - f(\vecz) = \underbrace{x_1 \cdot \partial_{x_1}f + \cdots + x_n \cdot \partial_{x_n}f}_{\text{Jacobian}} + \text{ higher order terms}
    \]\pause
	
	\vspace{-.75em}
	[Pandey-Saxena-Sinhababu]: Look up till the \textcolor{OliveGreen}{inseparable degree} in the expansion. \pause
	
	\vspace{1em}
	\begin{columns}
		\begin{column}{.55\textwidth}
			\textbf{A New Operator}: For any $f \in \F[\x]$,
            \[
                \Ech_t(f) = \deg^{\leq t} \inparen{f(\vecx + \vecz) - f(\vecz)}
            \]
		\end{column}\pause
		\begin{column}{.45\textwidth}
			\[\hat{\Ech}(\vecf) = \begin{bmatrix}
			& \ldots & \Ech_t(f_1) & \ldots & \\
			& \ldots & \Ech_t(f_2) & \ldots & \\
			& & \vdots\\
			& \ldots & \Ech_t(f_k) & \ldots &
			\end{bmatrix}\]
		\end{column}
	\end{columns}
    \vspace{-1em}
\end{frame}

\begin{frame}{Alternate Criterion for the General Case [Pandey-Saxena-Sinhababu]}
	$\fk \in \F[\vecx]$ are algebraically independent if and only if for every $\inparen{v_1, v_2, \ldots, v_k}$ with $v_i \text{s in } \I_t$, \pause
	\[\Ech(\vecf, \vecv) = \begin{bmatrix}
	& \ldots & \Ech_t(f_1) + v_1 & \ldots & \\
	& \ldots & \Ech_t(f_2) + v_2 & \ldots & \\
	& & \vdots\\
	& \ldots & \Ech_t(f_k) + v_k & \ldots &
	\end{bmatrix} \pause \text{ has full rank over } \F(\vecz)\]	\pause

	where $t$ is the \textcolor{OliveGreen}{inseparable degree} of $\set{\fk}$ and 
	\[
	    \I_t = \genset{\Ech_t(f_1), \Ech_t(f_2), \ldots, \Ech_t(f_k)}^{\geq 2}_{\F(\vecz)} \bmod{\genset{\vecx}^{t+1}} \subseteq \F(\vecz)[\vecx].
	\]
    \vspace{-2em}
\end{frame}

\begin{frame}{Our Result}
	\begin{tabbing}
		Suppose \quad\= $\circ$ $f_1,\ldots, f_m \in \F[x_1,\ldots, x_n]$\\
		\> $\circ$ algebraic rank of $\set{f_1,\ldots, f_m} = k$\\
		\> $\circ$ \textcolor{OliveGreen}{inseparable degree of $\set{f_1,\ldots, f_m} = t$}
	\end{tabbing}

	\pause Then, we can construct 
	\[
		\Phi:\F[\vecx] \rightarrow \F(s)[y_\zero, y_1,\ldots, y_k]
	\]\pause
	such that
	\[
		\algrank_\F(f_1 \circ \Phi, \ldots, f_m \circ \Phi) = k
	\]\pause
	whenever
	\begin{itemize}
		\item each of the $f_i$'s are sparse polynomials,
		\item each of the $f_i$'s are products of variable disjoint, multilinear, sparse polynomials.
	\end{itemize}
\end{frame}

\begin{frame}{Proof Overview}
	\textbf{Step 1}: Capture algebraic rank via linear rank of the \textcolor{OliveGreen}{PSS-Jacobian}\\ \pause
	
	\vspace{1em}
	\textbf{Step 2}: For a \emph{generic linear map} $\Phi: \vecx \rightarrow \F(s)[y_1,\ldots, y_k]$, write \textbf{PSS} $\J_\vecy(\vecf \circ \Phi)$ in terms of \textbf{PSS} $\J_\vecx(\vecf)$. \pause 
    This can be described succinctly as
	\[
		\text{\textbf{PSS} }\J_\vecy(f \circ \Phi) = \Phi(\text{\textbf{PSS} }\J_\vecx(\vecf)) \cdot M_\Phi. 
	\] \pause
	
	\vspace{-1em}
	\textbf{What we need:} $\Phi$ such that
	\begin{itemize}
		\item $\rank(\Phi(\text{\textbf{PSS} }\J_\vecx(\vecf))) = \rank(\text{\textbf{PSS} }\J_\vecx(\vecf))$: Can be done if $\vecf$'s are some structured polynomials (for example, \textcolor{OliveGreen}{sparse}). \pause
		\item $M_\Phi$ preserves rank. That is,
		
		\vspace{-.5em}
		\[
		\rank(\Phi(\text{\textbf{PSS} }\J_\vecx(\vecf)) \cdot M_\Phi) = \rank(\Phi(\text{\textbf{PSS} }\J_\vecx(\vecf))).
		\]
	\end{itemize}
\end{frame}

\begin{frame}{The Faithful Map}
	\begin{columns}
        \begin{column}{.4\textwidth}
            \begin{tikzpicture}
                \uncover<1->{\draw[thick] (0cm,0cm) rectangle (4cm,6cm); 
                \node(rows) at (-.5cm, 3cm) {$\vecx^\vece$};
                \draw[->, thick] ($(rows)+(0cm,.5cm)$) -- (-.5cm,6cm);
                \draw[->, thick] ($(rows)+(0cm,-.5cm)$) -- (-.5cm,0cm);
                \node(cols) at (2cm, -.5cm) {$\vecy^\vecd$};
                \draw[->, thick] ($(cols)+(.5cm,0cm)$) -- (4cm,-.5cm);
                \draw[->, thick] ($(cols)+(-.5cm,0cm)$) -- (0cm,-.5cm);
                \draw[thick] (.5cm,5.25cm) rectangle (0cm,6cm);
                \node(one) at (.25cm, 5.625cm) {$1$};
                \draw[thick] (.5cm,5.25cm) rectangle (1.25cm,4.25cm);
                \node(two) at (.825cm, 4.75cm) {$2$};
                \node(ddots1) at (1.5cm, 3.75cm) {$\ddots$};
                \node(ddots2) at (2cm, 3cm) {$\ddots$};
                \draw[thick] (2.5cm,2.5cm) rectangle (4cm,0cm);
                \node(tee) at (3.25cm, 1.25cm) {$t$};}
            \end{tikzpicture}
        \end{column}
        \begin{column}{.6\textwidth}
            \uncover<1->{$M_\Phi(\vecx^\vece, \vecy^\vecd) = \coeff_{\vecy^\vecd}(\Phi(\vecx^\vece))$}

            \vspace{2em}
		    \uncover<2->{\textbf{Taking inspiration from the previous case}:
            \[
                M_\Phi(x_i, y_j) = s^{\wt(i)j}  
            \]} 

            \vspace{1em}
		    \uncover<3->{For the correct definition of $\wt(i)$, things work out.}

            \vspace{1em}
		    \uncover<4->{
                \[
                    \color{BrickRed}{\Phi(x_i) = a_i \cdot y_\zero + \sum_{j \in [k]} s^{\wt(i)j} \cdot y_j}
                \]}
        \end{column}
    \end{columns}
    \vspace{-3em}
\end{frame}

\begin{frame}
	\begin{center}
		\large{\textbf{Questions?}}
	\end{center}
\end{frame}

\begin{frame}{Conclusion: Structure of the Thesis}
	\begin{enumerate}
		\item Introducing Algebraic Complexity Theory and the problems we study.
		\item A Quadratic Lower Bound Against ABPs.
		\item A Quadratic Lower Bound Against Formulas.
		\item Lower Bounds in the Non-Commutative Setting (including a proof which shows that [CILM] implies "super-quartic lower bounds against multilinear circuits would imply strong lower bounds against non-commutative circuits").
		\item Tight super-polynomial separation between abcd-Formulas and abcd-ABPs.
		\item Survey on Algebraic Independence Testing (including a proof which shows that the Jacobian Criterion continues to hold if the polynomials are at most quadratic).
		\item Constructing Faithful Maps over finite fields.
		\item Concluding remarks and open problems.
	\end{enumerate}
\end{frame}

\begin{frame}
	\begin{center}
		\large{\textbf{Thankyou!}}
	\end{center}
\end{frame}
\end{document}